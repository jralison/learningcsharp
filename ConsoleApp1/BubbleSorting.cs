﻿using System;

namespace ConsoleExercises {

    /**
     * From: www.worldbestlearningcenter.com/index_files/csharp-arrays-sorting-exercises.htm
     */
    internal class BubbleSorting {

        internal static void Run() {
            int[] numbers = new int[10];

            Console.WriteLine("Gerando números...");
            Random r = new Random();
            for (var n = 0; n < numbers.Length; n++)
                numbers[n] = r.Next(-100, 100);
            PrintOutNumbers(numbers);
            Console.WriteLine();

            Console.WriteLine("Ordenando os números...");
            Boolean swapped;
            do {
                swapped = false;
                for (int i = 0; i < numbers.Length; i++) {
                    for (int j = 0; j < numbers.Length - 1; j++) {
                        if (numbers[j] > numbers[j + 1]) {
                            Swap(ref numbers, j, (j + 1));
                        }
                    }
                }
            } while (swapped);
            PrintOutNumbers(numbers);
        }

        private static void Swap(ref int[] numbers, int pos_i, int pos_j) {
            int old_i = numbers[pos_i];
            numbers[pos_i] = numbers[pos_j];
            numbers[pos_j] = old_i;
        }

        private static void PrintOutNumbers(int[] numbers) {
            Console.Write("\t");
            foreach (int i in numbers)
                Console.Write("{0,4} ", i);
            Console.WriteLine();
        }
    }
}
