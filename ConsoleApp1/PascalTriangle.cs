﻿using System;

namespace ConsoleExercises {

    internal class PascalTriangle {

        internal static void Run() {

            uint triangleSize;
            do {
                Console.Write("Que dimensão do triângulo você deseja? ");
                string userOption = Console.ReadLine();
                if (uint.TryParse(userOption, out triangleSize))
                    if (triangleSize > 1)
                        break;
                    else
                        Console.WriteLine("{0} é um número muito curto. ", triangleSize);
                else if (string.IsNullOrEmpty(userOption))
                    return;
                else
                    Console.Write("Não entendi o número digitado. ");
                Console.WriteLine("Informe um inteiro maior que 1.");
            } while (true);
            Console.WriteLine();

            Console.WriteLine("Gerando triângulo de Pascal com {0} dimensões...", triangleSize);
            uint[][] pascal = new uint[triangleSize][];
            for (int i = 0; i < triangleSize; i++) {
                pascal[i] = new uint[i + 1];
                for (int j = 0; j <= i; j++) {
                    if (i < 2 || j < 1)
                        pascal[i][j] = 1;
                    else {
                        uint topLeft = pascal[i - 1][j - 1];
                        uint topRight = (i == j) ? 0 : pascal[i - 1][j];
                        pascal[i][j] = topLeft + topRight;
                    }
                }
            }

            if (triangleSize > 16)
                Console.WriteLine("(As últimas linhas podem aparecer quebradas na tela.)");
            Console.WriteLine("Imprimindo...");
            Console.WriteLine();
            for (int i = 0; i < pascal.Length; i++) {
                Console.Write("{0} ", (triangleSize > 16 ? "*" : " "));
                for (int j = 0; j < pascal[i].Length; j++) {
                    Console.Write("{0,6} ", pascal[i][j]);
                }
                Console.WriteLine();
            }
        }

    }

}
