﻿using System;

/**
 * From: www.worldbestlearningcenter.com/index_files/csharp-arrays-searching-exercises.htm
 */
namespace ConsoleExercises {

    internal class SequentialSearching {

        internal static void Run() {
            int minNum = 0,
                maxNum = 100;
            int[] numbers = new int[10];
            Random r = new Random();

            Console.WriteLine("Gerando lista...");
            Console.Write("\t");
            for (int i = 0; i < numbers.Length; i++) {
                numbers[i] = r.Next(minNum, maxNum);
                Console.Write(" {0,4} ", numbers[i]);
            }
            Console.WriteLine();

            Console.WriteLine();
            Console.Write("Digite um número entre {0} e {1} para pesquisar sua posição: ", minNum, maxNum);
            do {
                string userOption = Console.ReadLine();
                int selectedNumber;
                if (int.TryParse(userOption, out selectedNumber)) {
                    if (selectedNumber < minNum || selectedNumber > maxNum) {
                        Console.Write("Eu pedi um número entre {0} e {1}. Tente outra vez: ", minNum, maxNum);
                    } else {
                        Console.WriteLine();
                        Search(selectedNumber, ref numbers);
                        break;
                    }
                } else {
                    Console.Write("Não entendi o número digitado. Tente outro: ");
                }
            } while (true);
        }

        private static void Search(int search, ref int[] list) {
            int[] keys = new int[0];
            for (int n = 0; n < list.Length; n++) {
                if (list[n] == search) {
                    Array.Resize<int>(ref keys, keys.Length + 1);
                    keys[keys.Length - 1] = n;
                }
            }

            int nKeys = keys.Length;
            if (nKeys > 1) {
                string sKeys = "";
                for (int k = 0; k < nKeys; k++) {
                    if (k == nKeys - 2)
                        sKeys += keys[k] + " e ";
                    else if (k < nKeys - 2)
                        sKeys += keys[k] + ", ";
                    else
                        sKeys += keys[k];
                }
                Console.WriteLine("\tO número {0} foi encontrado {1} vezes. Nas posições {2}.", search, nKeys, sKeys);
            } else if (keys.Length == 1)
                Console.WriteLine("\tO número {0} foi encontrado na chave {1}.", search, keys[0]);
            else
                Console.WriteLine("\tO número {0} não está nesta lista.", search);
                
        }
    }

}
