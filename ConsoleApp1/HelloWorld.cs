﻿using System;

namespace ConsoleExercises {
    internal class HelloWorld {
        internal static void Run() {
            Console.WriteLine("Hello, World!");
        }
    }
}