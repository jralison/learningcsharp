﻿using System;

namespace ConsoleExercises {

    class Gateway {

        static void Main(string[] args) {
            Console.WriteLine("Bem-vindo ao meu caderno de exercícios do C#.");
            Console.WriteLine();

            string[] exercisesList = Enum.GetNames(typeof(Exercise));
            int selectedOption;
            do {
                DisplayMenu(exercisesList, out selectedOption);
                if (selectedOption > -1)
                    RunFor((Exercise)Enum.Parse(typeof(Exercise), exercisesList[selectedOption]));
            } while (selectedOption >= 0);
        }

        private static void DisplayMenu(string[] exercisesList, out int selectedOption) {
            Console.WriteLine("Que exercício você deseja executar agora?");
            Console.WriteLine(new String('=', Console.BufferWidth));

            foreach (var name in exercisesList)
                Console.WriteLine("{0,4}. {1}", (byte)Enum.Parse(typeof(Exercise), name), name);
            Console.WriteLine();

            string userOption;
            do {
                Console.Write("Escolha uma opção [Sair]: ");

                userOption = Console.ReadLine().Trim().ToUpper();
                if (string.IsNullOrEmpty(userOption)) {
                    selectedOption = -1;
                    break;
                } else if (!int.TryParse(userOption, out selectedOption) || (selectedOption < 0 || selectedOption >= exercisesList.Length)) {
                    Console.WriteLine();
                    Console.Write("Opção inválida. ");
                } else {
                    break;
                }
            } while (true);

            Console.Clear();
        }

        static void RunFor(Exercise option) {
            Console.WriteLine(Enum.GetName(typeof(Exercise), option));
            Console.WriteLine(new String('=', Console.BufferWidth));

            switch (option) {
                case Exercise.HelloWorld:
                    HelloWorld.Run();
                    break;
                case Exercise.RandomNumber:
                    RandomNumber.Run();
                    break;
                case Exercise.BubbleSorting:
                    BubbleSorting.Run();
                    break;
                case Exercise.SequentialSearching:
                    SequentialSearching.Run();
                    break;
                case Exercise.SubArrayAndTwoDimensionalArray:
                    SubArrayAndTwoDimensionalArray.Run();
                    break;
                case Exercise.PascalTriangle:
                    PascalTriangle.Run();
                    break;
                default:
                    throw new NotImplementedException();
            }

            // Repetir?
            Console.WriteLine();
            Console.Write(new String('=', Console.BufferWidth));
            do {
                Console.Write("Repetir? (S/N) [S]: ");
                string repetir = Console.ReadLine().Trim().ToUpper();
                if (string.IsNullOrEmpty(repetir) || repetir.StartsWith("S")) {
                    Console.Clear();
                    RunFor(option);
                    break;
                } else if (repetir.StartsWith("N")) {
                    Console.Clear();
                    break;
                } else
                    Console.Write("Não entendi... ");
            } while (true);
        }
    }

    enum Exercise : byte {
        HelloWorld,
        RandomNumber,
        BubbleSorting,
        SequentialSearching,
        SubArrayAndTwoDimensionalArray,
        PascalTriangle
    }

}
