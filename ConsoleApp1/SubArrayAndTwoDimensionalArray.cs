﻿using System;

namespace ConsoleExercises {

    internal class SubArrayAndTwoDimensionalArray {

        internal static void Run() {
            NumbersSeriesPrinting();
            Console.WriteLine(new String('-', Console.BufferWidth));
            MatrixPrinting();
        }

        private static void MatrixPrinting() {
            int[][] matrix = new int[5][];

            Console.WriteLine("Preenchendo a matriz ([i][j])...");
            for (int i = 0; i < matrix.Length; i++) {
                matrix[i] = new int[5];
                for (int j = 0; j < matrix.Length; j++) {
                    if (i > j)
                        matrix[i][j] = -1;
                    else if (i < j)
                        matrix[i][j] = 1;
                    else
                        matrix[i][j] = 0;
                }
            }

            Console.WriteLine("Imprimindo...");
            for (int i = 0; i < matrix.Length; i++) {
                Console.Write("\t");
                for (int j = 0; j < matrix[i].Length; j++)
                    Console.Write(" {0,5} ", matrix[i][j]);
                Console.WriteLine();
            }
        }

        private static void NumbersSeriesPrinting() {
            int[,] table = new int[5, 5];

            Console.WriteLine("Preenchendo o array de duas dimensões [i, j]...");
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    table[i, j] = (i * 5) + j + 1;
                }
            }

            Console.WriteLine("Imprimindo...");
            for (int i = 0; i < 5; i++) {
                Console.Write("\t");
                for (int j = 0; j < 5; j++)
                    Console.Write(" {0,5} ", table[i, j]);
                Console.WriteLine();
            }
        }
    }

}
