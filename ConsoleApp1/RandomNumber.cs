﻿using System;

namespace ConsoleExercises {

    /**
     * From: www.worldbestlearningcenter.com/index_files/csharp-random-exercises.htm
     */
    internal class RandomNumber {

        internal static void Run() {
            int random = new Random().Next(1, 6);

            Console.WriteLine("Pensei em um número inteiro de 1 a 6...");
            Console.Write("Que número você acha que pensei? ");
            string guess = Console.ReadLine();

            try {
                int iguess = int.Parse(guess);
                if (iguess > 6 || iguess < 1)
                    Console.WriteLine("\n\tIh, mané! Eu falei de 1 a 6...");
                else if (iguess == random)
                    Console.WriteLine("\n\tAew!!!! Você acertou!");
                else {
                    Console.WriteLine("\n\tPerdeu!!! HA-HAA");
                    Console.WriteLine("\tMeu número foi {0}.", random);
                }
            } catch (FormatException e) {
                Console.WriteLine("\n\tIsso não parece um número!");
                Console.Error.WriteLine(e.Message);
            }
        }
    }
}
